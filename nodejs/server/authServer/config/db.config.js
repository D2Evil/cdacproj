const { Sequelize } = require('sequelize');

const sequelize = new Sequelize({
  database: 'mydb',
  username: 'root',
  password: '1234',
  dialect: 'mysql',
});

sequelize
  .authenticate()
  .then(() => console.log('Connection has been established successfully.'))
  .catch(err => console.error('Unable to connect to the database:', err));



module.exports = sequelize;